/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seresvivos;
import java.util.*;
/**
 *
 * @author Ximena Puchaicela
 */
class PruebaSeres {

    public static void main(String[] args) {
        int opc;
        String nombre,mes;
        int dia,anio,edad;
        Scanner teclado = new Scanner(System.in);
        System.out.println("1.Loro\n2.Niño\n3.Adulto");opc = teclado.nextInt();
        switch(opc){
            case 1:
                System.out.println("Ingrese el nombre: ");nombre= teclado.next();
                SeresVivos loro= new Loro(nombre);
                System.out.println(loro);
                break;
            case 2:
                System.out.println("Ingrese el nombre: ");nombre = teclado.next();
                System.out.println("Ingrese la edad : "); edad = teclado.nextInt();
                SeresVivos ninio = new Ninio(nombre,edad);
                System.out.println(ninio);
                break;
  
            case 3:
                System.out.println("Ingrese el nombre: ");nombre = teclado.next();
                System.out.println("Ingrese dia de nacimiento: ");dia = teclado.nextInt();
                System.out.println("Ingrese mes de nacimiento: ");mes = teclado.next();
                System.out.println("Ingrese año de nacimiento: ");anio = teclado.nextInt();
                SeresVivos adulto = new Adulto(nombre,dia,mes,anio);
                System.out.println(adulto);
                break;
        }   
    }
}
public abstract class SeresVivos{
    public String  hablar;
    public abstract String hablar();
}
class Loro extends SeresVivos{
    String nombre;
    final String tipo = "no racional";
    public Loro(String nombre) {
        this.nombre = nombre;
    }
    public String hablar(){
        hablar = " Hola me llamo "+nombre+" y puedo hablar\n Soy un animal "+tipo;
        return hablar;
    }
    @Override
    public String toString() {
        return hablar();
    } 
}
class Ninio extends SeresVivos{
    String nombre;
    String tipo  = "racional";
    int edad ;
    public Ninio(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
    public String hablar(){
        hablar = "Hola, me llamo "+nombre+" y se hablar.\nSoy "+ tipo+"\nTengo "+edad+" años\nSoy un niño";
        return   hablar;
    }
    @Override
    public String toString() {
        return hablar();
    }
}
class Adulto extends SeresVivos{
    String nombre;
    String tipo  = "racional";
    int dia;
    String mes;
    int anio;
    int edad;
    public Adulto(){}
    public Adulto(String nombre, int dia, String mes, int anio) {
        this.nombre = nombre;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }
    public int edad(){
        edad = 2018-anio;
        return edad;
    }
    public String hablar(){
        hablar = "Hola, me llamo "+nombre+" y puedo hablar.\nSoy "+tipo+" .\nTengo "+edad()+" años.\nNaci el "+dia+""
                + " de "+mes+" de "+anio+"\nSoy adulto.";
        return hablar;
    }
    @Override
    public String toString() {
        return hablar();
    } 
}